const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const player = document.createElement("div")
player.id = "player";

for(let i=0; i<map.length; i++){
    let linha = document.createElement("div")
    linha.style.display = "flex";
    labirinto.appendChild(linha)

    for(let cel=0; cel<map[i].length; cel++){
        let celula = document.createElement("div")
        celula.dataset.local = i + "-" +cel
        let labirinto = document.getElementById("labirinto")
        if(map[i][cel] === "W"){
            celula.classList.add("parede");
        }
        if(map[i][cel] === "S"){
            celula.appendChild(player)

        }
        celula.classList.add("celStyle");
        linha.appendChild(celula)
        
    }
}

document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    let elementoPai = player.parentElement
    let elementoAvo = elementoPai.parentElement
    let local = elementoPai.dataset.local.split("-")
    let localY = Number(local[0])
    let localX = Number(local[1])

    if(keyName === "ArrowDown"){
        localY += 1
        player.style.animationName = "slideUp";
    }else if(keyName === "ArrowUp"){
        localY -= 1
        player.style.animationName = "slideUp";
    }else if(keyName === "ArrowRight"){
        localX += 1
        player.style.animationName = "slideRight";
        player.style.transform = "scaleX(1)";
    }else if(keyName === "ArrowLeft"){
        localX -= 1
        player.style.animationName = "slideLeft";
        player.style.transform = "scaleX(-1)";
    }

    elementoPai = document.querySelector(`div[data-local="${localY}-${localX}"]`);

    if(!elementoPai.className.includes("parede")){
        elementoPai.appendChild(player)
    }
    if(localY === 8 && localX === 20){
        showMessage()
    }
});

function showMessage(){
    console.log("GAHOU!")
    let vitoria = document.getElementById("vitoria");
    vitoria.style.display = "flex"
    setTimeout(function(){ document.location.reload(); }, 10000);
}

let yoshi = document.querySelector(`div[data-local="11-7"]`);
yoshi.classList.add("yoshi"); 
let princess = document.querySelector(`div[data-local="8-20"]`);
princess.classList.add("princess"); 